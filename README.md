# Final Project
This is our Rock Paper Scissors game by Prabina, Tabitha, Reeya, and Niraj.
We wrote it in Python, created a GUI with the help of Tkinter, and utilized 
LEDs and a physical push button. It successfully runs from the Raspberry Pi. 
We hope you enjoy! 

