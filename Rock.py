from tkinter import *
import tkinter.font as font
import random
import RPi.GPIO as GPIO
import time
from pygame import mixer

GPIO.setmode(GPIO.BCM)
GPIO.setwarnings(False)
GPIO.setup(18,GPIO.OUT)
GPIO.setup(24,GPIO.OUT)
GPIO.setup(16,GPIO.OUT)
GPIO.setup(26, GPIO.IN, pull_up_down=GPIO.PUD_UP) 

player_score = 0
computer_score = 0
options = [('Rock',0), ('Paper',1), ('Scissors',2)]

game_window = Tk()
game_window.title("Rock Paper Scissors Game")

def player_choice(player_input):
    global player_score, computer_score

    computer_input = get_computer_choice()

    player_choice_label.config(text = 'You Selected: ' + player_input[0])
    computer_choice_label.config(text = 'The Computer Selected: ' + computer_input[0])

    if(player_input == computer_input):
        winner_label.config(text = "It's a tie.", image = photoimage_tie, compound = LEFT)
        GPIO.output(16,GPIO.HIGH)
        time.sleep(1)
        GPIO.output(16,GPIO.LOW)

    elif((player_input[1] - computer_input[1]) % 3 == 1):
        player_score += 1
        winner_label.config(text="YOU WON!", image = photoimage_win, compound = LEFT)
        player_score_label.config(text = 'Your Score is: ' + str(player_score))
        GPIO.output(18,GPIO.HIGH)
        time.sleep(1)
        GPIO.output(18,GPIO.LOW)

    else:
        computer_score += 1
        winner_label.config(text="The computer won.", image = photoimage_lose, compound = LEFT)
        computer_score_label.config(text='Your Score: ' + str(computer_score))
        GPIO.output(24,GPIO.HIGH)
        time.sleep(1)
        GPIO.output(24,GPIO.LOW)

def get_computer_choice():
    return random.choice(options)

def button_callback(channel):
    game_window.destroy()

GPIO.add_event_detect(26,GPIO.RISING,callback=button_callback)
command = button_callback

mixer.init()
mixer.music.load("/home/pi/Rock/rockpaperscissors/bgmusic.wav")
mixer.music.play(-1)

app_font = font.Font(size = 12)

photo_win = PhotoImage(file = r"/home/pi/Rock/rockpaperscissors/win.png")
photoimage_win = photo_win.subsample(5, 5)

photo_lose = PhotoImage(file = r"/home/pi/Rock/rockpaperscissors/lose.png")
photoimage_lose = photo_lose.subsample(20, 20)

photo_tie = PhotoImage(file = r"/home/pi/Rock/rockpaperscissors/tie.png")
photoimage_tie = photo_tie.subsample(2, 2)


game_title = Label(text = 'Rock Paper Scissors', font = font.Font(size = 20), fg = 'red')
game_title.pack()

winner_label = Label(text = "Let's Start the Game", fg = 'blue', font = font.Font(size = 13), pady = 8)
winner_label.pack()

input_frame = Frame(game_window)
input_frame.pack()

player_options = Label(input_frame, text = "Your Options: ", font = app_font, fg = 'grey')
player_options.grid(row = 0, column = 0, pady = 8)

rock_btn = Button(input_frame, text = 'Rock', width = 15, bd = 0, bg = 'silver', pady = 5, command = lambda: player_choice(options[0]))
rock_btn.grid(row = 1, column = 1, padx = 8, pady = 5)

paper_btn = Button(input_frame, text = 'Paper', width = 15, bd = 0, bg = 'white', pady = 5, command = lambda: player_choice(options[1]))
paper_btn.grid(row = 1, column = 2, padx = 8, pady = 5)

scissors_btn = Button(input_frame, text = 'Scissors', width = 15, bd = 0, bg = 'green', pady = 5, command = lambda: player_choice(options[2]))
scissors_btn.grid(row = 1, column = 3, padx = 8, pady = 5)

done = Label(text='Press the Blue Button to Exit the Game', fg = 'blue')
done.pack()


score_label = Label(input_frame, text = 'Score: ', font = app_font, fg = 'yellow')
score_label.grid(row = 2, column = 0)

player_choice_label = Label(input_frame, text = 'You Selected: ---', font = app_font)
player_choice_label.grid(row = 3, column = 1, pady = 5)

player_score_label = Label(input_frame, text = 'Your Score: -', font = app_font)
player_score_label.grid(row = 3, column = 2, pady = 5)

computer_choice_label = Label(input_frame, text = 'Computer Selected: ---', font = app_font, fg = 'black')
computer_choice_label.grid(row = 4, column = 1, pady = 5)

computer_score_label = Label(input_frame, text = 'Computer Score: -', font = app_font, fg = 'black')
computer_score_label.grid(row = 4, column = 2, padx = (10,0), pady = 5)


game_window.geometry('800x400')
game_window.mainloop()
GPIO.cleanup()
